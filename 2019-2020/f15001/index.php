<?php

/**
 * kebab-case
 */
function stringToCamelCase(string $input): string
{
    // support more cases...

    $converted = array_map(function ($part) {
        return ucfirst($part)
    }, explode('-', $string));

    return implode('', $converted);
}