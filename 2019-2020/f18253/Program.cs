using System;
using System.Globalization;
using System.Linq;

namespace camelCase
{
    class Program
    {
   
        static string camelCase(string input)
        {
            TextInfo text = new CultureInfo("en-us", false).TextInfo;
            input = text.ToTitleCase(input).Replace("_", string.Empty).Replace(" ", string.Empty);
            input = $"{input.First().ToString().ToLowerInvariant()}{input.Substring(1)}";
            return input;
        }

        static void Main(string[] args)
        {
            string input, camel;

            Console.Write("Zadejte string: ");
            input = Console.ReadLine();
            camel = camelCase(input);
            Console.WriteLine("Výsledný string: " + camel);
            Console.ReadKey();

        }
    }
}
